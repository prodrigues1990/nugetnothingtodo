﻿using NUnit.Framework;

namespace Lib.Tests
{
    [TestFixture]
    public class MathTests
    {
        [Test]
        public void TestAdd()
        {
            Math m = new Math();
            Assert.AreEqual(m.Add(1, 2), m.Add(2, 1));
        }
    }
}
